;;; doom-smyck-theme.el --- based on the Smyck color scheme -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Author: Florian Posdziech <https://flowfx.de>
;; Created: October 27, 2021
;; Version: 1.0.0
;; Keywords: custom themes, faces
;; Homepage:
;; Package-Requires: ((emacs "25.1") (cl-lib "0.5") (doom-themes "2.2.1"))
;;
;;; Commentary:
;;
;; Built using the Smyck color scheme.
;;
;;; Code:

(require 'doom-themes)


;;
;;; Variables

(defgroup doom-smyck-theme nil
  "Options for the `doom-smyck' theme."
  :group 'doom-themes)

;;
;;; Theme definition

(def-doom-theme doom-smyck
  "A theme using the Smyck color scheme"

  ;; name        default   256           16
  (;; Smyck 0-7
   (black      '("#000000" "#000000" "black"      ))
   (red        '("#c75646" "#c75646" "red"        ))
   (green      '("#7da900" "#7da900" "green"      ))
   (yellow     '("#d0b03d" "#d0b03d" "yellow"     ))
   (smyck4     '("#407399" "#407399" "blue"       ))
   (violet     '("#ba8acc" "#ba8acc" "magenta"    ))
   (teal       '("#207383" "#207383" "cyan"       ))
   (light-grey '("#a1a1a1" "#a1a1a1" "brightblack"))
   ;; Smyck 8-15
   (grey          '("#4b4b4b" "#4b4b4b" "brightblack" ))
   (bright-red    '("#d6837c" "#d6837c" "brightred"   ))
   (bright-green  '("#c4f137" "#c4f137" "brightgreen" ))
   (bright-yellow '("#fee14d" "#fee14d" "brightyellow"))
   (smyck12       '("#8dcff0" "#8dcff0" "brightblue"  ))
   (magenta       '("#f79aff" "#f79aff" "magenta"     ))
   (cyan          '("#6ad9cf" "#207383" "cyan"        ))
   (white         '("#f7f7f7" "#f7f7f7" "brightwhite" ))
   ;; My preferred background color
   (smyck_bg '("#1d1d1d" "#1d1d1d" "black"  ))

   (bg smyck_bg )
   (fg white    )

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#21242b" "black"       "black"        ))
   (fg-alt     '("#5B6268" "#2d2d2d"     "white"        ))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#1B2229" "black"       "black"        ))
   (base1      '("#1c1f24" "#1e1e1e"     "brightblack"  ))
   (base2      '("#202328" "#2e2e2e"     "brightblack"  ))
   (base3      '("#23272e" "#262626"     "brightblack"  ))
   (base4      '("#3f444a" "#3f3f3f"     "brightblack"  ))
   (base5      '("#5B6268" "#525252"     "brightblack"  ))
   (base6      '("#73797e" "#6b6b6b"     "brightblack"  ))
   (base7      '("#9ca0a4" "#979797"     "brightblack"  ))
   (base8      '("#DFDFDF" "#dfdfdf"     "white"        ))

   (orange      bright-red)
   (blue        smyck12)
   (dark-blue   smyck4)
   (dark-cyan   teal)

   ;; These are the "universal syntax classes" that doom-themes establishes.
   ;; These *must* be included in every doom themes, or your theme will throw an
   ;; error, as they are used in the base theme defined in doom-themes-base.
   (highlight      blue)
   (vertical-bar   (doom-darken base1 0.1))
   (selection      dark-blue)
   (builtin        bright-green)
   (comments       light-grey)
   (doc-comments   light-grey)
   (constants      orange)
   (functions      blue)
   (keywords       bright-green)
   (methods        cyan)
   (operators      blue)
   (type           blue)
   (strings        yellow)
   (variables      dark-cyan)
   (numbers        yellow)
   (region         `(,(doom-lighten (car bg-alt) 0.15) ,@(doom-lighten (cdr base1) 0.35)))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red))

  ;; ;;;; Base theme face overrides
  ((org-block :background base2)
   (org-headline-todo :foreground white)
   (org-done :inherit 'org-headline-done :strike-through nil :weight: 'bold :height 1.0)
   (org-todo :inherit 'org-headline-todo :foreground green)

   (+org-todo-onhold :foreground yellow :weight 'normal)
   (+org-todo-project :foreground bright-red :weight 'normal)

   (org-link :inherit 'link :weight 'normal)

   (outline-1 :foreground teal :extend t)
   (outline-2 :foreground violet :extend t)
   (outline-3 :foreground dark-cyan :extend t)
   (outline-4 :foreground (doom-lighten teal 0.25) :extend t)
   (outline-5 :foreground (doom-lighten violet 0.25) :extend t)
   (outline-6 :foreground (doom-lighten dark-cyan 0.25) :extend t)
   (outline-7 :foreground (doom-lighten teal 0.5)  :weight 'bold :extend t)
   (outline-8 :foreground (doom-lighten violet 0.8)     :weight 'bold :extend t)
   ;; agenda
   (org-scheduled-today        :foreground yellow)
   (org-upcoming-near-deadline :foreground red))
  ;;;; Base theme variable overrides-
  ())

;;; doom-smyck-theme.el ends here
