(add-load-path! "lisp")

(setq user-full-name "Florian Posdziech"
      user-mail-address "hallo@flowfx.de")

(setq doom-font (font-spec :family "iA Writer Mono V" :size 11.0)
      doom-variable-pitch-font (font-spec :family "iA Writer Duo V" :size 11.0)
      )

(setq doom-theme 'doom-vibrant)

(custom-set-faces!
  '(font-lock-constant-face :foreground "#d6837c") ;; smyck9 brightred
  '(font-lock-function-name-face :foreground "#8dcff0") ;; smyck12 brightblue
  '(font-lock-keyword-face :foreground "#c4f137") ;; smyck10 brightgreen
  '(font-lock-string-face :foreground "#d0b03d") ;; smyck3 yellow
  '(highlight-numbers-number :foreground "#d0b03d" :weight normal) ;; smyck3 yellow
  '(font-lock-type-face :foreground "#8dcff0") ;; smyck12 brightblue
  )

(custom-set-faces
  '(default ((t (:background "#1c1c1c")))))

(after! org
  ;; TODO: extract org-directory into noweb reference so it can be used in all the places
  (setq org-directory "~/Things"
        org-agenda-files '(
                           "~/Things/todo.org"
                           "~/Things/calendars"
                           "~/Things/inbox.org"
                           )
        +org-capture-todo-file "inbox.org")

(setq org-todo-keywords '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "|" "DONE(d!)" "CANCELLED(c!)")))

(setq org-todo-keyword-faces '(("TODO" . (:foreground "#7bc275" :weight normal))
                               ("STARTED" . (:inherit warning :weight normal))
                               ("WAITING" . (:foreground "orange" :weight normal))))

(custom-set-faces!
  '(org-level-1 :inherit outline-1 :extend t :weight normal)
  '(org-level-2 :inherit outline-2 :extend t :weight normal)
  '(org-level-3 :inherit outline-3 :extend t :weight normal)
  '(org-level-4 :inherit outline-4 :extend t :weight normal)
  '(org-scheduled-today :foreground "#fcce7b") ;; warning yellow
  '(org-scheduled-previously :foreground "#ff665c") ;; error red
  '(org-imminent-deadline :foreground "#ff665c")
  '(org-upcoming-deadline :foreground "#fcce7b")
  '(org-checkbox-statistics-todo :inherit org-todo :weight normal)
  '(org-headline-todo :inherit org-level-2)
  )

(add-hook 'org-mode-hook 'mixed-pitch-mode)

(setq org-fontify-todo-headline nil)

(setq org-archive-location "~/Things/archive/%s_archive::"
      org-refile-targets '(("~/Things/todo.org" :maxlevel . 2)
                           ("~/Things/someday.org" :maxlevel . 2)
                           ("~/Things/bookmarks.org" :level . 0))
      )

(setq org-capture-templates
      '(
        ("t" "todo" entry (file +org-capture-todo-file) "* TODO %?")
        ("l" "literature note" plain (file my/generate-literature-note-name)
             "%(format \"#+TITLE: %s\n#+FILETAGS: :literature:book|post|video:\n\n\" my-literature-note--name)")
        ("b" "Bookmark" entry (file "~/Things/bookmarks.org")
             "* %(org-cliplink-capture) :%?:\n")
        ("p" "process email" entry (file +org-capture-todo-file)
             "* TODO %? %:fromname: %a")
   ))

(setq org-goto-interface 'outline-path-completion
      org-outline-path-complete-in-steps nil)

(setq org-reverse-note-order t)

(defun make-link-to-pull-request (pull_no)
  (browse-url (concat "https://github.com/dbdrive/triebwerk/pull/" pull_no)))

(defun make-link-to-issue (issue_no)
  (browse-url (concat "https://github.com/dbdrive/triebwerk/issues/" issue_no)))

(org-add-link-type "pr" #'make-link-to-pull-request)
(org-add-link-type "issue" #'make-link-to-issue)

(setq org-table-duration-hour-zero-padding nil)

(setq org-agenda-start-day nil                ;; start today
      org-agenda-span 'day                    ;; and show only today
      org-agenda-dim-blocked-tasks 'invisible ;; Don't show me any blocked todos. Next actions only - doesn't work for tags searches
      org-agenda-todo-ignore-scheduled 'future
      org-agenda-tags-todo-honor-ignore-options t
  ;;       ;; org-agenda-todo-ignore-deadlines 'near
  ;;       org-deadline-warning-days 3
)

(setq org-agenda-sorting-strategy '((agenda habit-down time-up priority-down category-keep)
                                    (todo todo-state-down priority-down)
                                    (tags todo-state-up priority-down)
                                    (search category-keep)))

(org-super-agenda-mode)
(setq org-agenda-overriding-header "")
(setq org-super-agenda-groups
        '(
          (:name "--------------------------------------------------\n 📥 INBOX" :category "inbox" :order 99)
          (:name "Due today" :deadline today :order 1 :face (:foreground "red" :background "black"))
          (:name "Due soon" :deadline future :order 2 :face (:foreground "red"))
          (:name "Today" :scheduled t :order 3 :face (:foreground "yellow"))
          (:name "Started" :todo "STARTED" :order 4)
          (:name "Current sprint" :and (:tag "sprint" :tag "current") :order 5)
          (:name "Retro topics" :and (:todo "TODO" :tag "retro"))
          (:name "Other" :anything t :order 50)
          ))

(setq org-agenda-custom-commands
      '(
        ("n" "This sprint or other urgent matters" tags-todo "-onhold-someday-retro-1on1")
        ("d" "Daily standup" tags-todo "daily")
        ("o" "1on1s" tags-todo "1on1")
        ("r" "Retro" tags-todo "retro")
        ))

;; (setq org-agenda-hide-tags-regexp ".*") ;; Hide all tags in agenda view
(setq org-agenda-block-separator 9472)     ;; Separate agenda and todos by a straight line
(setq org-agenda-skip-scheduled-if-done t) ;; Don't show done items in calendar
(setq org-agenda-entry-types '(:deadline :scheduled :timestamp :sexp)) ;; This is the default value
(setq org-agenda-skip-deadline-if-done t) ;; Don't show done items in agenda

(setq org-agenda-prefix-format
      '((agenda . " %i %?-12t% s")
        (todo . " %i ")
        (tags . " %i ")
        (search . " %i "))
      )

(setq org-super-agenda-header-map nil)
;; (setq org-super-agenda-hide-empty-groups t)

(after! org
  (setq org-re-reveal-title-slide nil)
  (setq org-re-reveal-root "/home/flowfx/bin/reveal.js/")
  )

(setq org-roam-directory (file-truename "~/Things/notes"))

(setq org-roam-capture-templates
  '(("d" "default" plain "%?" :target
     (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
     :unnarrowed t
     :jump-to-captured t)
    ("l" "literature note" plain "%?" :target
     (file+head "literature/${title}.org" "#+title: ${title}\n")
     :unnarrowed t
     :jump-to-captured t)))

(org-roam-db-autosync-mode)

)

(add-hook! prog-mode #'flymake-mode)

;; (after! lsp-mode
;;   (setq lsp-diagnostics-provider :flymake))

(defun my-compilation-mode-hook ()
  (setq truncate-lines nil) ;; automatically becomes buffer local
  (set (make-local-variable 'truncate-partial-width-windows) nil))

(add-hook 'rspec-compilation-mode-hook 'my-compilation-mode-hook)

(add-hook! 'ruby-mode 'robe-mode)

(setq rspec-use-spring-when-possible t)

(after! rspec-mode
  (defun rspec-runner ()
    "Return command line to run rspec."
    (let ((bundle-command (if (rspec-bundle-p) "bundle exec " ""))
          (zeus-command (if (rspec-zeus-p) "zeus " nil))
          (spring-command (if (rspec-spring-p) "bundle exec spring " nil))) ;; Original command is "spring "
      (concat (or zeus-command spring-command bundle-command)
              (if (rspec-rake-p)
                  (concat rspec-rake-command " spec")
                rspec-spec-command))))
  )

(setq rubocop-format-on-save nil)
(setq rubocop-autocorrect-on-save t)

(set-file-template! ".*\.md$" :trigger "__" :mode 'markdown-mode)
(set-file-template! "blog\/.*\.md$" :trigger "__zola-post" :mode 'markdown-mode) ;; Zola posts

(setq mail-user-agent 'mu4e-user-agent
    mu4e-sent-folder   "/Sent"
    mu4e-drafts-folder "/Drafts"
    mu4e-refile-folder "/Archive"
    mu4e-spam-folder   "/Junk"
    mu4e-trash-folder  "/Trash")

(setq mu4e-get-mail-command "true")

(setq mu4e-html2text-command "w3m -T text/html")

(setq mu4e-view-html-plaintext-ratio-heuristic most-positive-fixnum)



(setq
    message-send-mail-function   'smtpmail-send-it
    smtpmail-smtp-server         "smtp.mailbox.org"
    smtpmail-smtp-service        587
    smtpmail-smtp-stream-type    'starttls)

(defun my-mu4e-inbox ()
  "jump to mu4e inbox"
  (interactive)
  (mu4e~headers-jump-to-maildir "/Inbox"))

(map! :leader
      :desc "Jump to mu4e inbox"
      "oi" 'my-mu4e-inbox)

(define-key evil-motion-state-map (kbd "C-h") #'evil-window-left)
(define-key evil-motion-state-map (kbd "C-j") #'evil-window-down)
(define-key evil-motion-state-map (kbd "C-k") #'evil-window-up)
(define-key evil-motion-state-map (kbd "C-l") #'evil-window-right)

(global-set-key [f5] 'call-last-kbd-macro)

(defun my-org-work-agenda ()
  (interactive)
  (org-agenda nil "w"))

(defun my-org-inbox-file ()
  (interactive)
  (find-file "~/Things/inbox.org" ))

(map! :leader
      :desc "Open work agenda"
      "W" #'my-org-work-agenda)

(map! :leader
      :desc "Open my inbox"
      "I" #'my-org-inbox-file)

(key-chord-define evil-insert-state-map "jj" 'evil-normal-state)

(key-chord-mode 1)

(add-to-list 'safe-local-variable-values
             '(projectile-rails-custom-server-command . "bin/dev"))

(setq display-line-numbers-type 'relative)

(setq projectile-project-search-path '("~/Things/"))

(setq global-auto-revert-mode t)
;; (setq auto-revert-use-notify t)

(setq confirm-kill-emacs t)

(load! "lisp/keychain-environment")
(keychain-refresh-environment)

(load! "lisp/asdf")
(asdf-enable)

(load-file "~/Things/calendars/org-caldav-config.el")

(after! persp-mode
  (setq persp-emacsclient-init-frame-behaviour-override "main"))

(setq doom-modeline-persp-name t)

(setq auth-sources '("~/.authinfo"))

(setq orderless-matching-styles '(orderless-literal orderless-regexp orderless-flex))

(use-package vertico-repeat
  :after vertico
  :ensure nil)

(setq +zen-text-scale 1.3)

(setq anki-editor-ignored-org-tags '("literature"))
;; (anki-editor-mode)
